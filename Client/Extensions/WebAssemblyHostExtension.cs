﻿using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Microsoft.Extensions.DependencyInjection;
using System.Globalization;
using Microsoft.JSInterop;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Localization.Client.Extensions
{
    public static class WebAssemblyHostExtension
    {
        public async static Task SetDefaultCulture(this WebAssemblyHost host)
        {
            var jsInterop = host.Services.GetRequiredService<IJSRuntime>();
            var result = await jsInterop.InvokeAsync<string>("blazorCulture.get");

            CultureInfo culture;

            if(result != null)
            {
                culture = new CultureInfo(result);
            }
            else
            {
                if(CultureInfo.CurrentCulture.Name == "ko" || CultureInfo.CurrentCulture.Name == "ko-KR")
                {
                    culture = new CultureInfo("ko");
                }
                else
                {
                    culture = new CultureInfo(CultureInfo.CurrentCulture.Name);
                }
            }

            CultureInfo.DefaultThreadCurrentCulture = culture;
            CultureInfo.DefaultThreadCurrentUICulture = culture;
        }
    }
}
